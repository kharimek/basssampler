var bassSampler = {
  playSound: function(e) {    
    var pressedKey = e.keyCode;
    var availableKeys = document.querySelectorAll('audio[data-key]');
    var availableKeysNumbers = [];

    availableKeys.forEach(function(item) {      
      availableKeysNumbers.push(parseInt(item.dataset.key));       
    });

    if (availableKeysNumbers.indexOf(pressedKey) !== -1) {
      var soundSelector = 'audio[data-key="' + pressedKey + '"]';
      var soundToPlay = document.querySelector(soundSelector);
      soundToPlay.play();
    } else {
      this.showMsg('notFound');
    }
  },
  // stopAudio: function() {

  // },
  // stopAllAudio: function() {

  // },
  // showMsg: function(type) {
  //   switch (type) {
  //     case 'notFound':
  //       alert('key not found');
  //       break;
    
  //     default:
  //       break;
  //   }
  // }
}

var view = {
  setUpEventListeners: function() {
    document.addEventListener('keydown', function(e) {
      bassSampler.playSound(e);
    })
  }
}

view.setUpEventListeners();

